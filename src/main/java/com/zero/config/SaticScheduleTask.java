package com.zero.config;

import com.zero.mapper.MeetingRecordMapper;
import com.zero.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDateTime;

/**
 * @program: AiMeeting
 * @description:
 * @author: 周俊宇
 * @create: 2021-07-05 15:41
 **/
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class SaticScheduleTask {
    @Autowired
    MeetingService meetingService;

    //3.添加定时任务
    @Scheduled(cron = "0/5 * * * * ?")
    private void configureTasks() {
//        开始初始化
        System.out.println("开始初始化" + LocalDateTime.now());
         meetingService.InitMeeting();
    }
}