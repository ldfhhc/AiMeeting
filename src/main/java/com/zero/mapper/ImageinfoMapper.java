package com.zero.mapper;


import com.zero.Entity.ImageInfo;
import com.zero.Entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageinfoMapper {
    ImageInfo GetImgByUsername(String username);
    List<ImageInfo> GetAllImage();
    void InsertImage(ImageInfo imageInfo);
    void DeleteImageInfoByUsername(String username);
    Integer UpdateImageInfo(ImageInfo imageInfo);
}
