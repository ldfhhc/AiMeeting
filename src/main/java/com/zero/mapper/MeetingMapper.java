package com.zero.mapper;

import com.zero.Entity.Meeting;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: AiMeeting
 * @description: 会议持久层
 * @author: 周俊宇
 * @create: 2021-06-29 09:01
 **/
@Repository
public interface MeetingMapper {
    //    查找所有的会议
    List<Meeting> GetAllMeeting();

    //    根据会议Id查找会议
    Meeting GetMeetingByMeetingid(String meetingid);

    //    增加一个会议
    Integer InsertOneMeeting(Meeting meeting);

    //    更改会议状态
    Integer UpdateMeetingStatusByMeetingid(Integer status, String meetingid);

    //     更改会议开始时间
    Integer UpdateMeetingStartTimeByMeetingid(String starttimestamp, String meetingid);

    //     更改会议结束时间
    Integer UpdateMeetingEndTimeByMeetingid(String endtimestamp, String meetingid);

    //     会议人数加
    Integer AddCountsByMeetingid(String meetingid);

    //     会议人数减
    Integer DownCountsByMeetingid(String meetingid);

    //    根据用户用户名查找所有会议
    List<Meeting> GetAllMeetingByUsername(String username);

    //    修改会议
    Integer UpdateMeetingByMeeting(Meeting meeting);
}
