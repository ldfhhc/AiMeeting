package com.zero.mapper;

import com.zero.Entity.MeetingRecord;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: AiMeeting
 * @description: 参会退回相关Service
 * @author: 周俊宇
 * @create: 2021-06-29 15:28
 **/
@Repository
public interface MeetingRecordMapper {
    //    导出所有记录
    List<MeetingRecord> GetAllMeetingRecord();

    //  根据会议id查询参会人员记录
    MeetingRecord GetMeetingRecordByMeetingId(String meetingid);

    //   更改会议状态
    Integer ChangeMeetingStatusByUsernameAndMeetingId(Integer recordstatus, String username, String meetingid);

    //   设置签到时间
    Integer SetSignInTime(String signintimestamp, String username, String meetingid);

    //    设置签退时间
    Integer SetSignoutTime(String signouttimestamp, String username, String meetingid);

    //   设置参会状态(签到 签退)
    Integer Changerecordstatus(Integer recordstatus, String username, String meetingid);

    //  加入一个会议
    Integer InsertOneMeetingrecord(MeetingRecord meetingRecord);

    //  根据用户username查询参加的会议
    List<MeetingRecord> GetMeetingRecordByUsername(String username);

    //  根据用户username和会议号查询会议
    MeetingRecord GetMeetingRecordByUsernameAndMeetingid(String username,String meetingid);

    //   更改会议状态
    Integer ChangeMeetingRecordStatusByMeetingId(Integer recordstatus, String meetingid);
}
