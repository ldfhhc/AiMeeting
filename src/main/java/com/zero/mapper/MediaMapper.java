package com.zero.mapper;

import com.zero.Entity.Media;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: AiMeeting
 * @description: 语音持久层
 * @author: 周俊宇
 * @create: 2021-07-11 20:37
 **/
@Repository
public interface MediaMapper {
//    根据用户名和会议号查找该会议记录的语音记录
    List<Media> GetMediaByUsernameAndMeetingid(String username,String meetingid);
//    新增一条语音记录
     Integer JoinOneMedia(Media media);
//     修改一条语音
    Integer UpdateOneMediaByMediaid(Integer id,String MediaValue);
}
