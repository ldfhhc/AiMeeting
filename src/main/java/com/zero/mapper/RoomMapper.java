package com.zero.mapper;

import com.zero.Entity.Meeting;
import com.zero.Entity.Room;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: AiMeeting
 * @description: 会议持久层
 * @author: 周俊宇
 * @create: 2021-06-29 09:01
 **/
@Repository
public interface RoomMapper {
    List<Room> GetAllRoom();
    Room GetOneRoomByRoomId(Integer roomid);
}
