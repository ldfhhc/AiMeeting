package com.zero.MyEnum;

/**
 * @program: AiMeeting
 * @description:
 * @author: 周俊宇
 * @create: 2021-07-04 12:41
 **/
public enum IconEnum {
    calendar,
    add,
    comment,
    love,
    video
}
