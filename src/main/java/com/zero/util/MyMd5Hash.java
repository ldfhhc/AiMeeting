package com.zero.util;

import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.crypto.hash.SimpleHash;

public class MyMd5Hash {

    public String MyMd5HashToString(String Password)
    {
        String TempPassword = Password;
        String salt="AiMeeting";
        Integer hashIterations = 2;
        Md5Hash md5 = new Md5Hash(TempPassword);
        md5 =new Md5Hash(TempPassword,salt,hashIterations);
        SimpleHash hash = new SimpleHash("md5",TempPassword,salt,hashIterations);
        return hash.toString();
    }
}
