package com.zero.util;

import lombok.SneakyThrows;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @program: AiMeeting
 * @description: 用来处理时间字符串相关的逻辑
 * @author: 周俊宇
 * @create: 2021-06-29 13:59
 **/
public class MyTimeUtil {

    public static String TimestrToTimeStampstr(String Timestr) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = simpleDateFormat.parse(Timestr);
        return String.valueOf(date.getTime());
    }

    public static String TimeStampstrToTimestr(String TimeStampstr) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return simpleDateFormat.format(new Date(Long.parseLong(TimeStampstr)));
    }

    public static boolean CheckCreateTimeAndFishTime(String createtimestamp, String finishtimestamp) {
//        校验参数
        if (createtimestamp == null || createtimestamp.equals(""))
            return false;
        if (finishtimestamp == null || finishtimestamp.equals(""))
            return false;
//        验证参数
        Long nowtimestamp = new Date().getTime();
        if (nowtimestamp > Long.parseLong(createtimestamp) || nowtimestamp > Long.parseLong(finishtimestamp)) {
//            时间不满足真实逻辑
            return false;
        }
//        开始时间晚于结束时间
        if (Long.parseLong(createtimestamp) >= Long.parseLong(finishtimestamp))
            return false;
        return true;
    }

    @SneakyThrows
    public static boolean CheckFreeRoomTime(String starttime, String endtime, String targetstarttime, String targetendtime) {
        //        校验参数
        if (starttime == null || starttime.equals(""))
            return false;
        if (endtime == null || endtime.equals(""))
            return false;
        if (targetstarttime == null || targetstarttime.equals(""))
            return false;
        if (targetendtime == null || targetendtime.equals(""))
            return false;
//        转换参数类型
        Long starttime_long = Long.parseLong(TimestrToTimeStampstr(starttime));
        Long endtime_long = Long.parseLong(TimestrToTimeStampstr(endtime));
        Long targetstarttime_long = Long.parseLong(targetstarttime);
        Long targetendtime_long = Long.parseLong(targetendtime);
        //        验证参数
        //        根据定义逻辑完成参数校验
        if (starttime_long <= targetstarttime_long && endtime_long >= targetendtime_long)
        {
            return false;
        }
        if (starttime_long <= targetstarttime_long && (endtime_long >= targetendtime_long && endtime_long <= targetendtime_long))
        {
            return false;
        }
        if (starttime_long >= targetstarttime_long && endtime_long<=targetendtime_long)
        {
            return false;
        }
        if ((starttime_long<= targetendtime_long && starttime_long >= targetstarttime_long) && endtime_long>=targetendtime_long)
        {
            return false;
        }
        if (endtime_long < targetstarttime_long || starttime_long > targetendtime_long)
        {
            return true;
        }
        return false;
    }
}