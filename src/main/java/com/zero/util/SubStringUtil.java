package com.zero.util;

/**
 * @program: AiMeeting
 * @description: 取文本中间类
 * @author: 周俊宇
 * @create: 2021-06-24 14:34
 **/
public class SubStringUtil {

    public static String getBase64ImgType(String imgBase64code){
        return SubStringUtil.getSubString(imgBase64code.split(",")[0],"image/",";");
    }

    public static String getBase64Imgdata(String imgBase64code){
        return imgBase64code.split(",")[1];
    }


    /**
     * 取两个文本之间的文本值
     * @param text 源文本 比如：欲取全文本为 12345
     * @param left 文本前面
     * @param right  后面文本
     * @return 返回 String
     */
    public static String getSubString(String text, String left, String right) {
        String result = "";
        int zLen;
        if (left == null || left.isEmpty()) {
            zLen = 0;
        } else {
            zLen = text.indexOf(left);
            if (zLen > -1) {
                zLen += left.length();
            } else {
                zLen = 0;
            }
        }
        int yLen = text.indexOf(right, zLen);
        if (yLen < 0 || right == null || right.isEmpty()) {
            yLen = text.length();
        }
        result = text.substring(zLen, yLen);
        return result;
    }


}
