package com.zero.util;

import com.zero.Entity.ImageInfo;
import com.zero.Entity.ScoreEntity;
import com.zero.Entity.User;
import com.zero.FaceCompareUtil.WebFaceCompare;
import com.zero.mapper.ImageinfoMapper;
import com.zero.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @program: AiMeeting
 * @description: 一个用于人脸的工具类
 * @author: 周俊宇
 * @create: 2021-06-28 10:28
 **/
@Component
public class FaceUtil {
    @Autowired
    private static ImageinfoMapper imageinfoMapper;
    @Autowired
    private static UserMapper userMapper;

    @Autowired
    public FaceUtil(ImageinfoMapper imageinfoMapper,UserMapper userMapper) {
        this.userMapper = userMapper;
        this.imageinfoMapper = imageinfoMapper;
    }

    public static HashMap<String,Object> GetUserByImg(String img)
    {
//        WebFaceCompare webFaceCompare = new WebFaceCompare();
//        String imgdata = SubStringUtil.getBase64Imgdata(img);
//        String imgtype = SubStringUtil.getBase64ImgType(img);
//        List<ImageInfo> Allinfo = imageinfoMapper.GetAllImage();
//        for (ImageInfo imageInfo:Allinfo)
//        {
//            String TargetImgdata = SubStringUtil.getBase64Imgdata(imageInfo.getImg());
//            String TargetImgType = SubStringUtil.getBase64ImgType(imageInfo.getImg());
//            try {
//                Double Score = webFaceCompare.faceContrastByBase64(imgdata,imgtype,TargetImgdata,TargetImgType);
//                System.out.println(Score);
//                if (Score>0.80)
//                {
//                    User targetuser = userMapper.getuserbyusername(imageInfo.getUsername());
//                    return targetuser;
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return null;
//        创建返回对象
        HashMap<String,Object> reshashmap = new HashMap<>();
        reshashmap.put("user",null);
        reshashmap.put("message","未知错误");
        WebFaceCompare webFaceCompare = new WebFaceCompare();
        String imgdata = SubStringUtil.getBase64Imgdata(img);
        String imgtype = SubStringUtil.getBase64ImgType(img);
        List<ImageInfo> Allinfo = imageinfoMapper.GetAllImage();
        List<ScoreEntity> scoreEntityList = new ArrayList<>();
        for (ImageInfo imageInfo:Allinfo)
        {
            String TargetImgdata = SubStringUtil.getBase64Imgdata(imageInfo.getImg());
            String TargetImgType = SubStringUtil.getBase64ImgType(imageInfo.getImg());
            try {
                Double Score = webFaceCompare.faceContrastByBase64(imgdata,imgtype,TargetImgdata,TargetImgType);
                System.out.println(Score);
                ScoreEntity scoreEntity = new ScoreEntity(imageInfo.getUsername(),Score);
                scoreEntityList.add(scoreEntity);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Collections.sort(scoreEntityList);
        System.out.println(scoreEntityList);
        if (scoreEntityList.size() == 0) {
            reshashmap.put("message","请先添加人脸图片");
            return reshashmap;
        }
        ScoreEntity TargetSocreEntity = scoreEntityList.get(0);
        if (TargetSocreEntity.getScore() > 0.85) {
            reshashmap.put("user",userMapper.getuserbyusername(TargetSocreEntity.getUsername()));
            reshashmap.put("message",TargetSocreEntity.toString());
            return reshashmap;
        }
        else {
            reshashmap.put("message","请使用高清的人像进行验证");
            return reshashmap;
        }

    }
}
