package com.zero.util;

import com.zero.MyEnum.IconEnum;

import java.security.SecureRandom;
import java.util.Random;

/**
 * @program: AiMeeting
 * @description:
 * @author: 周俊宇
 * @create: 2021-07-04 12:43
 **/
public class EnumUtil {
    private static final SecureRandom random = new SecureRandom();
    public static <T extends Enum<?>> T randomEnum(Class<T> clazz){
        int x = random.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    public static void main(String[] args) {
        System.out.println(randomEnum(IconEnum.class));
    }
}
