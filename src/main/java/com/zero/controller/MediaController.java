package com.zero.controller;

import com.zero.Entity.Media;
import com.zero.Entity.Meeting;
import com.zero.mapper.MediaMapper;
import com.zero.mapper.UserMapper;
import com.zero.model.ResultMap;
import com.zero.service.MediaService;
import com.zero.service.RoomService;
import com.zero.util.JWTUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @program: AiMeeting
 * @description: 语音注释控制层
 * @author: 周俊宇
 * @create: 2021-07-11 20:49
 **/
@RestController
@Api("语音转写接口")
public class MediaController {

    private final UserMapper userMapper;
    private final ResultMap resultMap;
    private final MediaMapper mediaMapper;

    public MediaController(UserMapper userMapper, ResultMap resultMap, MediaMapper mediaMapper) {
        this.userMapper = userMapper;
        this.resultMap = resultMap;
        this.mediaMapper = mediaMapper;
    }

    @Autowired
    HttpServletRequest request;

    @Autowired
    MediaService mediaService;

    /*
    *
    * @Author 周俊宇
    * @Description //TODO 新增一个语音注释
    * @Date 2021/7/11
    * @Param
    * @return
    **/
    @PostMapping("/AddOneMedia")
    @ApiOperation(value = "新增一条会议记录", notes = "新增会议记录的内容到服务器")
    @RequiresRoles(logical = Logical.OR, value = {"user","admin"})
    public ResultMap AddOneMedia(@RequestBody Media media) {
        String username = JWTUtil.getUsername(request.getHeader("token"));
        if (media.getMediavalue().trim().equals(""))
        {
            return resultMap.fail().code(400).message("会议记录内容不能为空");
        }
        HashMap<String,Object> ResMap = mediaService.AddOneMedia(username,media.getMeetingid(),media.getMediavalue());
        if ((Integer)ResMap.get("status") == 1)
        {
            return resultMap.success().code(200).message(ResMap.get("message"));
        }
        else {
            return resultMap.fail().code(400).message(ResMap.get("message"));
        }
    }

    @PostMapping("/GetMediaList")
    @RequiresRoles(logical = Logical.OR, value = {"user","admin"})
    public ResultMap GetMediaList(@RequestBody Media media) {
        String username = JWTUtil.getUsername(request.getHeader("token"));
        if (media.getMeetingid().trim().equals(""))
        {
            return resultMap.fail().code(400).message("参数错误，请重新请求");
        }
        HashMap<String,Object> ResMap = mediaService.GetMediaList(username,media.getMeetingid());
        if ((Integer)ResMap.get("status") == 1)
        {
            return resultMap.success().code(200).message(ResMap.get("message"));
        }
        else {
            return resultMap.fail().code(400).message("参数错误,请重新登陆");
        }
    }
}
