package com.zero.controller;

import com.auth0.jwt.JWT;
import com.zero.Entity.ImageInfo;
import com.zero.Entity.User;
import com.zero.FaceCompareUtil.WebFaceCompare;
import com.zero.mapper.ImageinfoMapper;
import com.zero.mapper.UserMapper;
import com.zero.model.ResultMap;
import com.zero.util.JWTUtil;
import com.zero.util.MyMd5Hash;
import com.zero.util.SubStringUtil;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.HashMap;

@RestController
@RequestMapping("/user")
public class UserController {
    private final UserMapper userMapper;
    private final ResultMap resultMap;
    private final ImageinfoMapper imageinfoMapper;

    @Autowired
    HttpServletRequest request;

    @Autowired
    public UserController(UserMapper userMapper, ResultMap resultMap, ImageinfoMapper imageinfoMapper) {
        this.userMapper = userMapper;
        this.resultMap = resultMap;
        this.imageinfoMapper = imageinfoMapper;
    }

    /**
     * 拥有 user, admin 角色的用户可以访问下面的页面
     */
    @GetMapping("/getMessage")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap getMessage() {
        return resultMap.success().code(200).message("成功获得信息！");
    }


    @PostMapping("/updatePassword")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap updatePassword(@RequestBody HashMap<String,String> map) {
        String username = JWTUtil.getUsername(request.getHeader("token"));
        MyMd5Hash myMd5Hash = new MyMd5Hash();
        String dataBasePassword = userMapper.getPassword(username);
        if (dataBasePassword.equals(myMd5Hash.MyMd5HashToString(map.get("oldPassword")))) {
           Integer res = userMapper.updatePassword(username, myMd5Hash.MyMd5HashToString(map.get("newPassword")));
           if (res == 1)
           {
               return resultMap.success().code(200).message("修改密码成功");
           }
           else return resultMap.fail().code(400).message("密码修改失败！");
        } else {
            return resultMap.fail().code(400).message("密码错误！");
        }
    }

    @PostMapping("/setpassword")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap setpassword(@RequestBody HashMap<String,String> map) {
        String username = JWTUtil.getUsername(request.getHeader("token"));
        if (userMapper.updatePassword(username,new MyMd5Hash().MyMd5HashToString(map.get("newPassword"))) == 1)
        {
            return resultMap.success().code(200).message("更新成功");
        }
        else return resultMap.fail().code(400).message("更新失败");
    }

    @PostMapping("/LostPasswordChange")
    public ResultMap LostPasswordChange(@RequestBody HashMap<String,String> map) {
        if (map.get("username")==null || map.get("username").equals("")){
            return resultMap.fail().code(400).message("请输入用户名。");
        }
        String username = map.get("username");
        if (userMapper.updatePassword(username,new MyMd5Hash().MyMd5HashToString(map.get("newPassword"))) == 1)
        {
            return resultMap.success().code(200).message("更新成功");
        }
        else return resultMap.fail().code(400).message("更新失败");
    }

    @GetMapping("/GetInfo")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap GetInfo() {
        String username = JWTUtil.getUsername(request.getHeader("token"));
        User user = userMapper.getuserbyusername(username);
        if (user == null)
        {
            return  resultMap.fail().code(400).message("参数错误,请重新登陆");
        }
        HashMap<String,String> ResMap = new HashMap<>();
        ResMap.put("username",username);
        ResMap.put("realname",user.getRealname());
        return resultMap.success().code(200).message(ResMap);
    }


}
