package com.zero.controller;

import com.zero.Entity.Meeting;
import com.zero.Entity.MeetingListEntity;
import com.zero.mapper.*;
import com.zero.model.ResultMap;
import com.zero.service.MeetingService;
import com.zero.util.JWTUtil;
import com.zero.util.MyTimeUtil;
import lombok.SneakyThrows;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @program: AiMeeting
 * @description: 会议相关控制器
 * @author: 周俊宇
 * @create: 2021-06-29 10:03
 **/
@RestController
public class MeetingController {
    private final UserMapper userMapper;
    private final ResultMap resultMap;
    private final ImageinfoMapper imageinfoMapper;
    private final MeetingMapper meetingMapper;
    private final MeetingRecordMapper meetingRecordMapper;
    private final RoomMapper roomMapper;

    @Autowired
    HttpServletRequest request;

    @Autowired
    MeetingService meetingService;

    public MeetingController(UserMapper userMapper, ResultMap resultMap, ImageinfoMapper imageinfoMapper, MeetingMapper meetingMapper, MeetingRecordMapper meetingRecordMapper, RoomMapper roomMapper) {
        this.userMapper = userMapper;
        this.resultMap = resultMap;
        this.imageinfoMapper = imageinfoMapper;
        this.meetingMapper = meetingMapper;
        this.meetingRecordMapper = meetingRecordMapper;
        this.roomMapper = roomMapper;
    }

    @SneakyThrows
    @PostMapping("/CreateMeeting")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap CreateMeeting(@RequestBody Meeting meeting) {
        String username = JWTUtil.getUsername(request.getHeader("token"));
        ReentrantLock reentrantLock = new ReentrantLock();
        reentrantLock.lock();
        HashMap<String,Object> ResMap = meetingService.CreateMeeting(username,meeting);
        reentrantLock.unlock();
        if ((Integer)ResMap.get("status") != 1)
        {
            return resultMap.fail().code(400).message(ResMap.get("message"));
        }
        else {
            return  resultMap.success().code(200).message(ResMap.get("message"));
        }
    }

    /*
     *
     * @Author 周俊宇
     * @Description //TODO 加入某个会议
     * @Date 2021/6/29
     * @Param
     * @return
     **/
    @PostMapping("/JoinOneMeeting")
    @RequiresRoles(logical = Logical.OR, value = {"user","admin"})
    public ResultMap JoinOneMeeting(@RequestBody Meeting meeting) {
        String username = JWTUtil.getUsername(request.getHeader("token"));
        if (meeting.getMeetingid().equals(""))
        {
            return resultMap.fail().code(400).message("会议id不能为空");
        }
        HashMap<String,Object> ResMap = meetingService.JoinOneMeeting(username,meeting.getMeetingid());
        if ((Integer)ResMap.get("status") == 1)
        {
            return resultMap.success().code(200).message(ResMap.get("message"));
        }
        else {
            return resultMap.fail().code(400).message(ResMap.get("message"));
        }
    }

    @GetMapping("/GetMeetingList")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap JoinOneMeeting() {
        String username = JWTUtil.getUsername(request.getHeader("token"));
        HashSet<MeetingListEntity> meetingset = meetingService.GetMeetingList(username);
        System.out.println(meetingset);
        return resultMap.success().code(200).message(meetingset);
    }

    @PostMapping("/MeetingSignOut")
    @RequiresRoles(logical = Logical.OR, value = {"admin"})
    public ResultMap MeetingSignOut(@RequestBody HashMap<String,String> map) {
        String username = JWTUtil.getUsername(map.get("username"));
//       校验参数
        if (map == null || map.get("meetingid").equals(""))
        {
            return resultMap.fail().code(400).message("参数错误,请重新提交");
        }
//        将请求交给Service处理 用一个HashMap接收处理结果
        HashMap<String,Object> ResMap = new HashMap<>();
        ResMap = meetingService.MeetingSignOut(map.get("username"), map.get("meetingid"));
        if ((Integer) ResMap.get("status") == 1)
        {
            return resultMap.success().code(200).message(ResMap.get("message"));
        }
        else {
            return resultMap.fail().code(400).message(ResMap.get("message"));
        }
    }

    @PostMapping("/MeetingSignIn")
    @RequiresRoles(logical = Logical.OR, value = {"admin"})
    public ResultMap MeetingSignIn(@RequestBody HashMap<String,String> map) {
        String username = JWTUtil.getUsername(map.get("username"));
//       校验参数
        if (map == null || map.get("meetingid").equals(""))
        {
            return resultMap.fail().code(400).message("参数错误,请重新提交");
        }
//        将请求交给Service处理 用一个HashMap接收处理结果
        HashMap<String,Object> ResMap = new HashMap<>();
        ResMap = meetingService.MeetingSignIn(map.get("username"), map.get("meetingid"));
        if ((Integer) ResMap.get("status") == 1)
        {
            return resultMap.success().code(200).message(ResMap.get("message"));
        }
        else {
            return resultMap.fail().code(400).message(ResMap.get("message"));
        }
    }

    @SneakyThrows
    @PostMapping("/GetMeetingInfo")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap GetMeetingInfo(@RequestBody HashMap<String,String> map) {
        System.out.println(map);
        if ( map.get("meetingid") == null || map.get("meetingid").equals(""))
        {
            return resultMap.fail().code(400).message("参数错误,请重新请求");
        }
        HashMap<String,Object> ResMap = new HashMap<>();
        Meeting TargetMeeting = meetingMapper.GetMeetingByMeetingid(map.get("meetingid"));
        if (TargetMeeting == null)
        {
            return resultMap.fail().code(400).message("会议不存在,请检查会议id");
        }
        TargetMeeting.setCreatetimestamp(MyTimeUtil.TimeStampstrToTimestr(TargetMeeting.getCreatetimestamp()));
        TargetMeeting.setFinishtimestamp(MyTimeUtil.TimeStampstrToTimestr(TargetMeeting.getFinishtimestamp()));
        if (!TargetMeeting.getStarttimestamp().equals("未开始"))
        {
            TargetMeeting.setStarttimestamp(MyTimeUtil.TimeStampstrToTimestr(TargetMeeting.getStarttimestamp()));
        }
        if (!TargetMeeting.getEndtimestamp().equals("未结束"))
        {
            TargetMeeting.setEndtimestamp(MyTimeUtil.TimeStampstrToTimestr(TargetMeeting.getEndtimestamp()));
        }
        ResMap.put("targetmeeting",TargetMeeting);
        ResMap.put("room",roomMapper.GetOneRoomByRoomId(TargetMeeting.getRoomid()).getRoomname());
        return resultMap.success().code(200).message(ResMap);
    }
}
