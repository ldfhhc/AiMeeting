package com.zero.controller;

import com.zero.Entity.ImageInfo;
import com.zero.mapper.ImageinfoMapper;
import com.zero.mapper.UserMapper;
import com.zero.model.ResultMap;
import com.zero.util.JWTUtil;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.crypto.hash.Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.util.HashMap;

@RestController
@RequestMapping("/ImageInfo")
public class ImageInfoController {
    private final UserMapper userMapper;
    private final ResultMap resultMap;
    private final ImageinfoMapper imageinfoMapper;

    @Autowired
    HttpServletRequest request;

    @Autowired
    public ImageInfoController(UserMapper userMapper, ResultMap resultMap, ImageinfoMapper imageinfoMapper) {
        this.userMapper = userMapper;
        this.resultMap = resultMap;
        this.imageinfoMapper = imageinfoMapper;
    }

    /*
     *
     * @Author 周俊宇
     * @Description //TODO
     * @Date 2021/6/24
     * @Param 根据用户Token查找该用户人脸图
     * @return
     **/
    @PostMapping("/getImageInfoByUsername")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap getImageInfoByUsername() {
        String username = JWTUtil.getUsername(request.getHeader("Token"));
        if (username == null || username.equals("")) {
            return resultMap.fail().code(401).message("请重新登陆");
        }
        return resultMap.success().code(200).message(imageinfoMapper.GetImgByUsername(username));
    }

    @GetMapping("/getAllImageInfo")
    @RequiresRoles(logical = Logical.OR, value = {"admin"})
    public ResultMap getAllImageInfo() {
        return resultMap.success().code(200).message(imageinfoMapper.GetAllImage());
    }

    @PostMapping("/InsertOneImageInfo")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap InsertImageInfo(@RequestBody HashMap<String, String> map) {
        if (map == null) {
            return resultMap.fail().code(401).message("请正确提交参数");
        }
        if (map.get("img").equals("")) {
            return resultMap.fail().code(401).message("参数不能为空");
        }
        String username = JWTUtil.getUsername(request.getHeader("Token"));
        if (username == null || username.equals("")) {
            return resultMap.fail().code(401).message("请重新登陆");
        }
        if (imageinfoMapper.GetImgByUsername(username) != null)
        {
            return resultMap.fail().code(401).message("用户信息已存在，如需修改请使用更新功能");
        }
        ImageInfo imageInfo = new ImageInfo();
        imageInfo.setUsername(username);
        imageInfo.setImg(map.get("img"));
        imageinfoMapper.InsertImage(imageInfo);
        return resultMap.success().code(200).message("提交成功!");
    }

    @PostMapping("/DeleteImageInfo")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap DeleteImageInfo() {
        String username = JWTUtil.getUsername(request.getHeader("Token"));
        imageinfoMapper.DeleteImageInfoByUsername(username);
        return resultMap.success().code(200).message("删除成功!");
    }

    @PostMapping("/UpdateImageInfo")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap UpdateImageInfo(@RequestBody HashMap<String,String> map) {
        if (map == null) {
            return resultMap.fail().code(401).message("请正确提交参数");
        }
        if (map.get("img").equals("")) {
            return resultMap.fail().code(401).message("参数不能为空");
        }
        String username = JWTUtil.getUsername(request.getHeader("Token"));
        if (username == null || username.equals("")) {
            return resultMap.fail().code(401).message("请重新登陆");
        }
        ImageInfo imageInfo = new ImageInfo();
        imageInfo.setUsername(username);
        imageInfo.setImg(map.get("img"));
        if (imageinfoMapper.UpdateImageInfo(imageInfo) == 1)
        {
            return resultMap.success().code(200).message("更新成功!");
        }
        else{
            return resultMap.fail().code(400).message("更新失败");
        }
    }

    @PostMapping("/findbyusernametest")
//    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap findbyusernametest(@RequestBody HashMap<String,String> map) {
       return resultMap.success().code(400).message(userMapper.getuserbyusername(map.get("username")));
    }

}
