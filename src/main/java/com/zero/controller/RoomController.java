package com.zero.controller;

import com.zero.Entity.Meeting;
import com.zero.mapper.MeetingMapper;
import com.zero.mapper.UserMapper;
import com.zero.model.ResultMap;
import com.zero.service.MeetingService;
import com.zero.service.RoomService;
import com.zero.util.JWTUtil;
import lombok.SneakyThrows;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @program: AiMeeting
 * @description: 用来检查可用会议室的Controller
 * @author: 周俊宇
 * @create: 2021-07-09 11:25
 **/
@RestController
@RequestMapping("/room")
public class RoomController {
    private final UserMapper userMapper;
    private final ResultMap resultMap;
    private final MeetingMapper meetingMapper;

    public RoomController(UserMapper userMapper, ResultMap resultMap, MeetingMapper meetingMapper) {
        this.userMapper = userMapper;
        this.resultMap = resultMap;
        this.meetingMapper = meetingMapper;
    }

    @Autowired
    HttpServletRequest request;

    @Autowired
    RoomService roomService;

    @PostMapping("/GetFreeRoom")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap CreateMeeting(@RequestBody HashMap<String,String> map) {
        String username = JWTUtil.getUsername(request.getHeader("token"));
        HashMap<String,Object> ResMap = roomService.GetFreeRoom(map.get("starttime"),map.get("endtime"));
        if ((Integer)ResMap.get("status") != 1)
        {
            return resultMap.fail().code(400).message(ResMap.get("message"));
        }
        else {
            return resultMap.success().code(200).message(ResMap.get("message"));
        }
    }
}