package com.zero.controller;

import com.zero.Entity.ImageInfo;
import com.zero.Entity.ScoreEntity;
import com.zero.Entity.User;
import com.zero.FaceCompareUtil.WebFaceCompare;
import com.zero.mapper.ImageinfoMapper;
import com.zero.mapper.UserMapper;
import com.zero.model.ResultMap;
import com.zero.util.FaceUtil;
import com.zero.util.JWTUtil;
import com.zero.util.SubStringUtil;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.crypto.hash.Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.lang.annotation.Target;
import java.util.*;
import java.util.List;

/**
 * @program: AiMeeting
 * @description: 人脸认证相关
 * @author: 周俊宇
 * @create: 2021-06-26 17:21
 **/
@RestController
@RequestMapping("/face")
public class FaceController {
    private final UserMapper userMapper;
    private final ResultMap resultMap;
    private final ImageinfoMapper imageinfoMapper;

    @Autowired
    HttpServletRequest request;

    @Autowired
    public FaceController(UserMapper userMapper, ResultMap resultMap, ImageinfoMapper imageinfoMapper) {
        this.userMapper = userMapper;
        this.resultMap = resultMap;
        this.imageinfoMapper = imageinfoMapper;
    }

    @GetMapping("/Comparetest")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap Comparetest() {
        WebFaceCompare webFaceCompare = new WebFaceCompare();
        String username = JWTUtil.getUsername(request.getHeader("token"));
        ImageInfo MyImageInfo = imageinfoMapper.GetImgByUsername(username);
        if (MyImageInfo == null)
        {
            return resultMap.fail().code(400).message("请先录入人脸");
        }
        String imgdata = SubStringUtil.getBase64Imgdata(MyImageInfo.getImg());
        String imgtype = SubStringUtil.getBase64ImgType(MyImageInfo.getImg());
        List<ImageInfo> Allinfo = imageinfoMapper.GetAllImage();
        List<ScoreEntity> scoreEntityList = new ArrayList<>();
        for (ImageInfo imageInfo:Allinfo)
        {
            String TargetImgdata = SubStringUtil.getBase64Imgdata(imageInfo.getImg());
            String TargetImgType = SubStringUtil.getBase64ImgType(imageInfo.getImg());
            try {
                Double Score = webFaceCompare.faceContrastByBase64(imgdata,imgtype,TargetImgdata,TargetImgType);
                System.out.println(Score);
                ScoreEntity scoreEntity = new ScoreEntity(imageInfo.getUsername(),Score);
                scoreEntityList.add(scoreEntity);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Collections.sort(scoreEntityList);
        System.out.println(scoreEntityList);
        if (scoreEntityList.size() == 0) {
            return resultMap.fail().code(500).message("请先添加人脸图片");
        }
        ScoreEntity TargetSocreEntity = scoreEntityList.get(0);
        if (TargetSocreEntity.getScore() > 0.80 && TargetSocreEntity.getUsername().equals(username)) {
            return resultMap.success().code(200).message(TargetSocreEntity);
        }
        else {
            return resultMap.fail().code(400).message("请使用高清的人像进行验证");
        }
    }

    @PostMapping("/LostPasswordFaceCheck")
    public ResultMap LostPasswordFaceCheck(@RequestBody HashMap<String,String> map) {
        if (map == null)
            return resultMap.fail().code(400).message("参数错误");
//        实现根据用户名和人脸验证找到用户信息
        if (map.get("username").equals(""))
        {
             return resultMap.fail().code(400).message("用户名参数为空");
        }
        if (map.get("img").equals(""))
        {
            return resultMap.fail().code(400).message("人像数据为空");
        }
//        定义操作者身份
        String username = map.get("username");
        HashMap<String, Object>  mymap = FaceUtil.GetUserByImg(map.get("img"));
        User targetuser = (User) mymap.get("user");
        String message = (String) mymap.get("message");
        if (targetuser == null)
        {
            return resultMap.fail().code(400).message(message);
        }
        else
        {
           if (targetuser.getUsername().equals(username)){
               HashMap<String,String> ResMap = new HashMap<>();
               ResMap.put("message","验证成功");
               ResMap.put("uname",username);
//               匹配成功
               return resultMap.success().code(200).message(ResMap);
           }
           else
           {
               return resultMap.fail().code(400).message("验证失败,请使用高清人脸照片进行验证");
           }
        }
    }

    @PostMapping("/SignFaceCheck")
    public ResultMap SignFaceCheck(@RequestBody HashMap<String,String> map) {
        if (map == null)
            return resultMap.fail().code(400).message("参数错误");
        if (map.get("img").equals(""))
        {
            return resultMap.fail().code(400).message("人像数据为空");
        }
        HashMap<String, Object>  mymap = FaceUtil.GetUserByImg(map.get("img"));
        User targetuser = (User) mymap.get("user");
        String message = (String) mymap.get("message");
        if (targetuser == null)
        {
            return resultMap.fail().code(400).message(message);
        }
        else
        {
            HashMap<String,String> ResMap = new HashMap<>();
            ResMap.put("message","匹配用户成功");
            ResMap.put("uname",targetuser.getUsername());
            return resultMap.success().code(200).message(ResMap);
        }
    }

    @PostMapping("/FaceCheckOnToken")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap FaceCheckOnToken(@RequestBody HashMap<String,String> map) {
        if (map == null)
            return resultMap.fail().code(400).message("参数错误");
        if (map.get("img").equals(""))
        {
            return resultMap.fail().code(400).message("人像数据为空");
        }
//        定义操作者身份
        String username = JWTUtil.getUsername(request.getHeader("token"));

        HashMap<String, Object>  mymap = FaceUtil.GetUserByImg(map.get("img"));
        User targetuser = (User) mymap.get("user");
        String message = (String) mymap.get("message");
        if (targetuser == null)
        {
            return resultMap.fail().code(400).message(message);
        }
        else
        {
            if (targetuser.getUsername().equals(username)){
//               匹配成功
                return resultMap.success().code(200).message("验证成功");
            }
            else
            {
                return resultMap.fail().code(400).message("验证失败,请使用高清人脸照片进行验证");
            }
        }
    }

    @GetMapping("/faceisexist")
    @RequiresRoles(logical = Logical.OR, value = {"user", "admin"})
    public ResultMap Faceisexist() {
        String username = JWTUtil.getUsername(request.getHeader("token"));
        ImageInfo imageInfo = imageinfoMapper.GetImgByUsername(username);
        if (imageInfo==null)
            return resultMap.fail().code(400).message("人脸信息不存在，请按要求录入人脸信息");
        if (!imageInfo.getImg().equals(""))
            return resultMap.success().code(200).message("人脸信息存在");
        return resultMap.fail().code(400).message("未知错误!");
    }

}

