package com.zero.controller;


import com.zero.mapper.UserMapper;
import com.zero.model.ResultMap;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/admin")
public class AdminController {
    private final UserMapper userMapper;
    private final ResultMap resultMap;

    @Autowired
    public AdminController(UserMapper userMapper, ResultMap resultMap) {
        this.userMapper = userMapper;
        this.resultMap = resultMap;
    }

    /**
     *
     * @param
     * @return com.zero.SchoolQR.model.ResultMap
     * @author ZhouJunYu
     * @creed: 获取用户列表
     * @date 2021/6/8 15:16
     */
    @GetMapping("/getUser")
    @RequiresRoles("admin")
    public ResultMap getUser() {
        List<String> list = userMapper.getUser();
        return resultMap.success().code(200).message(list);
    }
    /*
    *
    * @Author 周俊宇
    * @Description //封号模块
    * @Date 2021/6/24
    * @Param 用户名
    * @return
    **/
    @PostMapping("/banUser")
    @RequiresRoles("admin")
    public ResultMap banuser(String username) {
        userMapper.banUser(username);
        return resultMap.success().code(200).message("成功封号！");
    }


}
