package com.zero.controller;


import com.zero.Entity.User;
import com.zero.mapper.UserMapper;
import com.zero.model.ResultMap;
import com.zero.util.FaceUtil;
import com.zero.util.JWTUtil;
import com.zero.util.MyMd5Hash;
import org.apache.shiro.crypto.hash.Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@RestController
public class LoginController {
    private final UserMapper userMapper;
    private final ResultMap resultMap;

    @Autowired
    public LoginController(UserMapper userMapper, ResultMap resultMap) {
        this.userMapper = userMapper;
        this.resultMap = resultMap;
    }


    @PostMapping("/register")
    public ResultMap register(@RequestBody HashMap<String,String> map) {
        if (userMapper.IsExist(map.get("username")) == null) {
            MyMd5Hash myMd5Hash = new MyMd5Hash();
            User UserObject = new User();
//            引用数据
            UserObject.setUsername(map.get("username"));
            UserObject.setPassword(myMd5Hash.MyMd5HashToString(map.get("password")));
            UserObject.setRole("user");
            UserObject.setPermission("normal");
            UserObject.setBan(0);
            UserObject.setRealname(map.get("realname"));
            userMapper.InsertUser(UserObject);
            return resultMap.success().code(200).message("注册成功！");
        } else {
            return resultMap.fail().code(402).message("账户已存在");
        }
    }

    @PostMapping("/login")
    public ResultMap login(@RequestBody HashMap<String, String> map) {
        HashMap<String,String> ResMap = new HashMap<>();
        ResMap.put("message","未知错误");
        ResMap.put("role",null);
        String realPassword = userMapper.getPassword(map.get("username"));
        MyMd5Hash myMd5Hash = new MyMd5Hash();
        if (realPassword == null) {
            ResMap.put("message","用户名错误");
            return resultMap.fail().code(401).message(ResMap);
        } else if (!realPassword.equals(myMd5Hash.MyMd5HashToString(map.get("password")))) {
            ResMap.put("message","密码错误");
            return resultMap.fail().code(401).message(ResMap);
        } else {
            User targetuser = userMapper.getuserbyusername(map.get("username"));
            if (targetuser.getBan() != 0)
            {
                ResMap.put("message","该账号已被禁用，如有疑问请联系管理员");
                return resultMap.fail().code(400).message(ResMap);
            }
            else{
                ResMap.put("message",JWTUtil.createToken(map.get("username")));
                ResMap.put("role",targetuser.getRole());
                return resultMap.success().code(200).message(ResMap);
            }
        }
    }

    @PostMapping("/loginbyface")
    public ResultMap loginbyface(@RequestBody HashMap<String, String> map) {
        if (map.get("img").equals("")) {
            return resultMap.fail().code(400).message("人脸图片为空，请重新上传");
        }
        Map<String, Object> resmap = new HashMap<>();
        resmap = FaceUtil.GetUserByImg(map.get("img"));
        User resuser = (User) resmap.get("user");
        String resmessage = (String) resmap.get("message");
        if (resuser == null) {
            return resultMap.fail().code(400).message(resmessage);
        }
        String username = resuser.getUsername();
        HashMap<String,String> ResMap = new HashMap<>();
        ResMap.put("message",JWTUtil.createToken(username));
        ResMap.put("uname",username);
        ResMap.put("role",resuser.getRole());
        return resultMap.success().code(200).message(ResMap);
//        完成认证
    }

}
