package com.zero.service;

import org.apache.shiro.crypto.hash.Hash;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * @program: AiMeeting
 * @description: 会议室SerVice层
 * @author: 周俊宇
 * @create: 2021-07-09 11:41
 **/
@Service
public interface RoomService {
    public HashMap<String,Object> GetFreeRoom(String starttime,String endtime);
}
