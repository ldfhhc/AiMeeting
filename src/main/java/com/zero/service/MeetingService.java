package com.zero.service;

import com.zero.Entity.Meeting;
import com.zero.Entity.MeetingListEntity;
import com.zero.model.ResultMap;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.HashSet;

/**
 * @program: AiMeeting
 * @description: 会议Service层
 * @author: 周俊宇
 * @create: 2021-07-01 09:26
 **/
@Service
public interface MeetingService {
    public HashMap<String,Object> JoinOneMeeting(String username, String meetingid);
    public HashMap<String,Object> CreateMeeting(String username,Meeting meeting);
    public HashSet<MeetingListEntity> GetMeetingList(String username);
    public HashMap<String,Object> MeetingSignOut(String username,String meetingid);
    public HashMap<String,Object> MeetingSignIn(String username,String meetingid);
    public void InitMeeting();
}
