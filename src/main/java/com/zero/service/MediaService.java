package com.zero.service;

import com.zero.Entity.MeetingListEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;

/**
 * @program: AiMeeting
 * @description: 语音Service层
 * @author: 周俊宇
 * @create: 2021-07-11 20:35
 **/
@Service
public interface MediaService {
    //    根据用户名和会议id获取所有的语音记录
    public HashMap<String,Object> GetMediaList(String username,String meetingid);
//   新增一个会议
    public HashMap<String,Object> AddOneMedia(String username,String meetingid,String mediavalue);
}
