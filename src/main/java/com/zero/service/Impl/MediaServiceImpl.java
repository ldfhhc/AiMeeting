package com.zero.service.Impl;

import com.zero.Entity.Media;
import com.zero.Entity.MeetingRecord;
import com.zero.MyEnum.IconEnum;
import com.zero.mapper.*;
import com.zero.model.ResultMap;
import com.zero.service.MediaService;
import com.zero.service.MeetingService;
import com.zero.util.EnumUtil;
import com.zero.util.MyTimeUtil;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @program: AiMeeting
 * @description:
 * @author: 周俊宇
 * @create: 2021-07-11 20:35
 **/
@Service
public class MediaServiceImpl implements MediaService {
    @Autowired
    private static MediaMapper mediaMapper;

    @Autowired
    private static MeetingRecordMapper meetingRecordMapper;

    @Autowired
    public MediaServiceImpl(MediaMapper mediaMapper, MeetingRecordMapper meetingRecordMapper) {
        this.mediaMapper = mediaMapper;
        this.meetingRecordMapper = meetingRecordMapper;
    }

    @SneakyThrows
    @Override
    public HashMap<String, Object> GetMediaList(String username,String meetingid) {
        HashMap<String, Object> ResMap = new HashMap<>();
        ResMap.put("message",null);
        ResMap.put("status","-1");
        List<Media> mediaList = mediaMapper.GetMediaByUsernameAndMeetingid(username,meetingid);
//        对列表时间戳转时间
        List<Media> newmediaList = new ArrayList<>();
        for (Media media:mediaList)
        {
            media.setCreatetimestamp(MyTimeUtil.TimeStampstrToTimestr(media.getCreatetimestamp()));
            media.setIcon(EnumUtil.randomEnum(IconEnum.class).toString());
            newmediaList.add(media);
        }
        ResMap.put("message",newmediaList);
        ResMap.put("status",1);
        return ResMap;
    }

    @Override
    public HashMap<String, Object> AddOneMedia(String username, String meetingid,String mediavalue) {
//        新增一个语音记录
        HashMap<String, Object> ResMap = new HashMap<>();
        ResMap.put("message","未知错误!");
        ResMap.put("status","-1");
//        校验参数
        MeetingRecord targetmeetingRecord = meetingRecordMapper.GetMeetingRecordByUsernameAndMeetingid(username,meetingid);
        if (targetmeetingRecord == null)
        {
            ResMap.put("message","您未参加该会议");
            ResMap.put("status","0");
            return ResMap;
        }
//        创建一个语音对象
        Media media = new Media();
        media.setMeetingid(meetingid);
        media.setMediavalue(mediavalue);
        media.setCreatetimestamp(String.valueOf(new Date().getTime()));
        media.setUsername(username);
        if (mediaMapper.JoinOneMedia(media) == 1){
            ResMap.put("message","添加成功!");
            ResMap.put("status",1);
            return ResMap;
        }
        else {
            ResMap.put("message","添加失败!");
            ResMap.put("status",0);
            return ResMap;
        }
    }


}
