package com.zero.service.Impl;

import com.zero.Entity.Meeting;
import com.zero.Entity.Room;
import com.zero.mapper.MeetingMapper;
import com.zero.mapper.MeetingRecordMapper;
import com.zero.mapper.RoomMapper;
import com.zero.service.RoomService;
import com.zero.util.MyTimeUtil;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @program: AiMeeting
 * @description: 会议室Service实现类
 * @author: 周俊宇
 * @create: 2021-07-09 11:42
 **/
@Service
public class RoomServiceImpl implements RoomService {
    @Autowired
    private static MeetingMapper meetingMapper;
    @Autowired
    private static RoomMapper roomMapper;

    @Autowired
    public RoomServiceImpl(MeetingMapper meetingMapper, RoomMapper roomMapper) {
        this.meetingMapper = meetingMapper;
        this.roomMapper = roomMapper;
    }


    @SneakyThrows
    @Override
    public HashMap<String, Object> GetFreeRoom(String starttime, String endtime) {
//        定义返回体
        HashMap<String, Object> ResMap = new HashMap<>();
        ResMap.put("message", "未知错误");
        ResMap.put("status", -1);
//        校验逻辑参数
        System.out.println(starttime);
        System.out.println(endtime);
        if (Long.parseLong(MyTimeUtil.TimestrToTimeStampstr(starttime)) < new Date().getTime())
        {
            ResMap.put("message", "会议开始时间不能早于当前时间。");
            ResMap.put("status", 0);
            return ResMap;
        }
        if (Long.parseLong(MyTimeUtil.TimestrToTimeStampstr(starttime)) > Long.parseLong(MyTimeUtil.TimestrToTimeStampstr(endtime)))
        {
            ResMap.put("message", "会议开始时间不能晚于会议结束时间");
            ResMap.put("status", 0);
            return ResMap;
        }
//        获取所有房间对象
        List<Room> roomList = roomMapper.GetAllRoom();
//        定义一个List用来存放冲突的会议室
        List<Room> conflictingroomlist = new ArrayList<>();
//        获取所有会议
        List<Meeting> meetingList = meetingMapper.GetAllMeeting();
//       遍历房间对象
        for (Meeting meeting:meetingList)
        {
            if (MyTimeUtil.CheckFreeRoomTime(starttime,endtime,meeting.getCreatetimestamp(),meeting.getFinishtimestamp()) == false && meeting.getMeetingstatus() != 2)
            {
//            与该会议时间冲突 获取该会议的会议室号
                conflictingroomlist.add(roomMapper.GetOneRoomByRoomId(meeting.getRoomid()));
            }
        }
//        计算可用会议室
        roomList.removeAll(conflictingroomlist);
        System.out.println(meetingList);
        ResMap.put("message", roomList);
        ResMap.put("status", 1);
        return ResMap;
    }
}
