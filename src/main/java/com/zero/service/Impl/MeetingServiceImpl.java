package com.zero.service.Impl;


import com.zero.Entity.Meeting;
import com.zero.Entity.MeetingListEntity;
import com.zero.Entity.MeetingRecord;
import com.zero.Entity.Room;
import com.zero.MyEnum.IconEnum;
import com.zero.mapper.MeetingMapper;
import com.zero.mapper.MeetingRecordMapper;
import com.zero.mapper.RoomMapper;
import com.zero.service.MeetingService;
import com.zero.service.RoomService;
import com.zero.util.EnumUtil;
import com.zero.util.MyTimeUtil;
import lombok.SneakyThrows;
import org.apache.shiro.crypto.hash.Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.*;

/**
 * @program: AiMeeting
 * @description: 会议接口实现
 * @author: 周俊宇
 * @create: 2021-07-01 09:29
 **/
@Service
public class MeetingServiceImpl implements MeetingService {
    @Autowired
    private static MeetingMapper meetingMapper;
    @Autowired
    private static MeetingRecordMapper meetingRecordMapper;
    @Autowired
    private static RoomMapper roomMapper;

    @Autowired
    RoomService roomService;

    @Autowired
    public MeetingServiceImpl(MeetingMapper meetingMapper, MeetingRecordMapper meetingRecordMapper,RoomMapper roomMapper) {
        this.meetingMapper = meetingMapper;
        this.meetingRecordMapper = meetingRecordMapper;
        this.roomMapper = roomMapper;
    }

    @Override
    public HashMap<String, Object> JoinOneMeeting(String username, String meetingid) {
//        定义返回体
        HashMap<String, Object> ResMap = new HashMap<>();
        ResMap.put("message", "未知错误");
        ResMap.put("status", -1);
        //        校验有没有参加其他会议
        List<MeetingRecord> meetingRecords = meetingRecordMapper.GetMeetingRecordByUsername(username);
        for (MeetingRecord meetingRecord : meetingRecords) {
            if (meetingRecord.getMeetingid().equals(meetingid)) {
                ResMap.put("message", "请不要重复参加会议");
                ResMap.put("status", 0);
                return ResMap;
            }
            if (meetingRecord.getRecordstatus() == 1) {
                ResMap.put("message", "当前还有未结束的会议");
                ResMap.put("status", 0);
                return ResMap;
            }
        }
        if (meetingid.equals("")) {
            ResMap.put("message", "会议id为空，请重新输入会议id");
            ResMap.put("status", 0);
            return ResMap;
        }

        //        先检查该会议在会议中是否存在， 是否已经过期。
        Meeting checkMeeting = meetingMapper.GetMeetingByMeetingid(meetingid);
        if (checkMeeting == null) {
            ResMap.put("message", "会议不存在");
            ResMap.put("status", 0);
            return ResMap;
        }
        if (checkMeeting.getMeetingstatus() == 2) {
            ResMap.put("message", "会议已结束");
            ResMap.put("status", 0);
            return ResMap;
        }
//        if (checkMeeting.getMeetingstatus() == 0) {
//            ResMap.put("message", "会议未开始");
//            ResMap.put("status", 0);
//            return ResMap;
//        }
//        当前时间早于会议开始时间不得参会 晚于参会时间不得参会
        if (new Date().getTime() > Long.parseLong(checkMeeting.getFinishtimestamp())) {
            ResMap.put("message", "请不要在非法时间加入会议");
            ResMap.put("status", 0);
            return ResMap;
        }
//        设置参会记录参数
        MeetingRecord meetingRecord = new MeetingRecord();
        meetingRecord.setMeetingid(meetingid);
        meetingRecord.setJointimestamp(String.valueOf(new Date().getTime()));
        meetingRecord.setSignintimestamp("未签到");
        meetingRecord.setSignouttimestamp("未签退");
        meetingRecord.setRecordstatus(0);
        meetingRecord.setUsername(username);
//        参会参数设置完毕
        if (meetingRecordMapper.InsertOneMeetingrecord(meetingRecord) == 1) {
//            if (checkMeeting.getStarttimestamp().equals("未开始")) {
////                当前为第一个参会 设置会议开始时间
//                meetingMapper.UpdateMeetingStartTimeByMeetingid(String.valueOf(new Date().getTime()), meetingid);
////                设置会议状态为开会中
//                meetingMapper.UpdateMeetingStatusByMeetingid(1, meetingid);
//            }
////            参会成功 参会人数自增
//            meetingMapper.AddCountsByMeetingid(meetingid);
            ResMap.put("message", "参会成功，已加入会议 [" + checkMeeting.getMeetingname() + "]");
            ResMap.put("status", 1);
            return ResMap;
        } else {
            ResMap.put("message", "加入会议失败!");
            ResMap.put("status", 0);
            return ResMap;
        }

    }

    @Override
    @SneakyThrows
    public HashMap<String, Object> CreateMeeting(String username, Meeting meeting) {
        Meeting TargetMeeting = new Meeting();
        HashMap<String, Object> ResMap = new HashMap<>();
//        检测会议名称是否为空
        if (meeting.getMeetingname() == null || meeting.getMeetingname().equals("")) {
            ResMap.put("message", "会议名称不能为空");
            ResMap.put("status", 0);
            return ResMap;
        }
        //        设置相关参数
        TargetMeeting.setCreateusername(username);
        TargetMeeting.setMeetingstatus(0);
        //        校验时间是否符合要求    开始时间不得大于结束时间  开始时间和结束时间不得早于当前时间
        if (MyTimeUtil.CheckCreateTimeAndFishTime(MyTimeUtil.TimestrToTimeStampstr(meeting.getCreatetimestamp()), MyTimeUtil.TimestrToTimeStampstr(meeting.getFinishtimestamp())) == false) {
            ResMap.put("message", "请检查时间是否填写正确");
            ResMap.put("status", 0);
            return ResMap;
        }
        try {
            TargetMeeting.setCreatetimestamp(MyTimeUtil.TimestrToTimeStampstr(meeting.getCreatetimestamp()));
            TargetMeeting.setFinishtimestamp(MyTimeUtil.TimestrToTimeStampstr(meeting.getFinishtimestamp()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TargetMeeting.setStarttimestamp("未开始");
        TargetMeeting.setEndtimestamp("未结束");
        TargetMeeting.setMeetingid(UUID.randomUUID().toString());
        TargetMeeting.setMeetingname(meeting.getMeetingname());
        TargetMeeting.setMeetingstatus(0);
//        在创建会议时在此对房间id进行校验 检测是否可用
//        先拿到当前可用房间
        List<Room> LocalFreeRoomList = (List<Room>) roomService.GetFreeRoom(meeting.getCreatetimestamp(),meeting.getFinishtimestamp()).get("message");
//         根据房间id拿到该会议室对象
        Room targetroom = roomMapper.GetOneRoomByRoomId(meeting.getRoomid());

        if (!LocalFreeRoomList.contains(targetroom))
        {
            ResMap.put("message", "选择会议室在当前时间段不可用,请重新选择");
            ResMap.put("status", 0);
            return ResMap;
        }
        TargetMeeting.setRoomid(meeting.getRoomid());
        //        设置完成
        Integer updateres = meetingMapper.InsertOneMeeting(TargetMeeting);
        if (updateres == 1) {
            /*增加成功*/
//            将会议创建者加入该会议
            this.JoinOneMeeting(username,TargetMeeting.getMeetingid());
//          返回会议创建的结果
            ResMap.put("message", TargetMeeting.getMeetingid());
            ResMap.put("status", 1);
//            返回会议id
            return ResMap;
        } else {
            ResMap.put("message", "创建会议失败");
            ResMap.put("status", 0);
            return ResMap;
        }
    }

    /*
     *
     * @Author 周俊宇
     * @Description //TODO 获取和某个人相关的会议列表
     * @Date 2021/7/1
     * @Param
     * @return
     **/
    @Override
    public HashSet<MeetingListEntity> GetMeetingList(String username) {
        HashSet<MeetingListEntity> hashSet = new HashSet<MeetingListEntity>();
//        先检测所有会议 创建者为自己的
        List<Meeting> meetings = meetingMapper.GetAllMeetingByUsername(username);
        for (Meeting meeting : meetings) {
            MeetingListEntity meetingListEntity = new MeetingListEntity();
            meetingListEntity.setMeetingid(meeting.getMeetingid());
            meetingListEntity.setMeetingname(meeting.getMeetingname());
            meetingListEntity.setCreateusername(meeting.getCreateusername());
            try {
                meetingListEntity.setCreatetimestamp(MyTimeUtil.TimeStampstrToTimestr(meeting.getCreatetimestamp()));
                meetingListEntity.setFinishtimestamp(MyTimeUtil.TimeStampstrToTimestr(meeting.getFinishtimestamp()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            meetingListEntity.setMeetingstatus(meeting.getMeetingstatus());
            meetingListEntity.setMeetingaddress(roomMapper.GetOneRoomByRoomId(meeting.getRoomid()).getRoomname());
            meetingListEntity.setIcon(EnumUtil.randomEnum(IconEnum.class).toString());
            hashSet.add(meetingListEntity);
        }
        List<MeetingRecord> meetingRecords = meetingRecordMapper.GetMeetingRecordByUsername(username);
        for (MeetingRecord meetingRecord : meetingRecords) {
            MeetingListEntity meetingListEntity = new MeetingListEntity();
            Meeting TargetMeeting = meetingMapper.GetMeetingByMeetingid(meetingRecord.getMeetingid());
            meetingListEntity.setMeetingid(meetingRecord.getMeetingid());
            meetingListEntity.setMeetingname(TargetMeeting.getMeetingname());
            meetingListEntity.setCreateusername(TargetMeeting.getCreateusername());
            try {
                meetingListEntity.setCreatetimestamp(MyTimeUtil.TimeStampstrToTimestr(TargetMeeting.getCreatetimestamp()));
                meetingListEntity.setFinishtimestamp(MyTimeUtil.TimeStampstrToTimestr(TargetMeeting.getFinishtimestamp()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            meetingListEntity.setMeetingstatus(TargetMeeting.getMeetingstatus());
            meetingListEntity.setMeetingaddress(roomMapper.GetOneRoomByRoomId(TargetMeeting.getRoomid()).getRoomname());
            meetingListEntity.setIcon(EnumUtil.randomEnum(IconEnum.class).toString());
            hashSet.add(meetingListEntity);
        }
        System.out.println(hashSet);
        return hashSet;
    }

    /*
     *
     * @Author 周俊宇
     * @Description //TODO 实现用户签退的逻辑
     * @Date 2021/7/1
     * @Param
     * @return
     **/
    @Override
    public HashMap<String, Object> MeetingSignOut(String username, String meetingid) {
//       用户签退 定义HashMap接收结果
        HashMap<String, Object> ResMap = new HashMap<>();
        ResMap.put("message", "未知错误");
        ResMap.put("status", -1);
//        先检查meetingid是否合法
        Meeting targetmeeting = meetingMapper.GetMeetingByMeetingid(meetingid);
        MeetingRecord targetmeetingRecord = meetingRecordMapper.GetMeetingRecordByUsernameAndMeetingid(username,meetingid);
        if (targetmeeting == null) {
            ResMap.put("message", "会议不存在，请检查meetingid");
            ResMap.put("status", 0);
            return ResMap;
        }
        if (targetmeetingRecord ==null)
        {
            ResMap.put("message", "您未参加该会议，无法签退");
            ResMap.put("status", 0);
            return ResMap;
        }
//        检测会议状态
        if (targetmeetingRecord.getSignintimestamp().equals("未签到")){
            ResMap.put("message", "您未签到，无法签退");
            ResMap.put("status", 0);
            return ResMap;
        }
//        检查会议时间是否合理
        if (new Date().getTime() < Long.parseLong(targetmeeting.getCreatetimestamp()) || new Date().getTime() > Long.parseLong(targetmeeting.getFinishtimestamp())) {
            ResMap.put("message", "签退时间不合法");
            ResMap.put("status", 0);
            return ResMap;
        }
//        校验会议状态
        if (targetmeeting.getMeetingstatus() == 2) {
            ResMap.put("message", "会议已结束");
            ResMap.put("status", 0);
            return ResMap;
        }
        if (!meetingRecordMapper.GetMeetingRecordByUsernameAndMeetingid(username,meetingid).getSignouttimestamp().equals("未签退")){
            ResMap.put("message", "请不要重复签退");
            ResMap.put("status", 0);
            return ResMap;
        }
        if (meetingRecordMapper.SetSignoutTime(String.valueOf(new Date().getTime()), username, meetingid) == 1) {
//            设置签退时间成功
//            接下来继续设置该会议参会人数自减
            meetingMapper.DownCountsByMeetingid(meetingid);
            meetingRecordMapper.ChangeMeetingStatusByUsernameAndMeetingId(0, username, meetingid);
            if (meetingMapper.GetMeetingByMeetingid(meetingid).getCounts() == 0) {
//                会议已经没人了 结束该会议 设置会议状态为结束
                meetingMapper.UpdateMeetingStatusByMeetingid(2, meetingid);
                meetingMapper.UpdateMeetingEndTimeByMeetingid(String.valueOf(new Date().getTime()), meetingid);
            }
            ResMap.put("message", "签退成功");
            ResMap.put("status", 1);
            return ResMap;
        } else {
            ResMap.put("message", "签退失败");
            ResMap.put("status", 0);
            return ResMap;
        }
    }

    @Override
    public HashMap<String, Object> MeetingSignIn(String username, String meetingid) {
        //       用户签到 定义HashMap接收结果
        HashMap<String, Object> ResMap = new HashMap<>();
        ResMap.put("message", "未知错误");
        ResMap.put("status", -1);
//        先检查meetingid是否合法
        Meeting targetmeeting = meetingMapper.GetMeetingByMeetingid(meetingid);
        MeetingRecord targetmeetingRecord = meetingRecordMapper.GetMeetingRecordByUsernameAndMeetingid(username,meetingid);
        if (targetmeeting == null) {
            ResMap.put("message", "会议不存在，请检查meetingid");
            ResMap.put("status", 0);
            return ResMap;
        }
//        检查是否参加该会议
        if (targetmeetingRecord ==null)
        {
            ResMap.put("message", "您未参加该会议，无法签退");
            ResMap.put("status", 0);
            return ResMap;
        }
//        检查会议时间是否合理
        if (new Date().getTime() < Long.parseLong(targetmeeting.getCreatetimestamp()) || new Date().getTime() > Long.parseLong(targetmeeting.getFinishtimestamp())) {
            ResMap.put("message", "签到时间不合法");
            ResMap.put("status", 0);
            return ResMap;
        }
//        校验会议状态
        if (targetmeeting.getMeetingstatus() == 2) {
            ResMap.put("message", "会议已结束");
            ResMap.put("status", 0);
            return ResMap;
        }
        if (!meetingRecordMapper.GetMeetingRecordByUsernameAndMeetingid(username,meetingid).getSignintimestamp().equals("未签到")){
            ResMap.put("message", "请不要重复签到");
            ResMap.put("status", 0);
            return ResMap;
        }
        if (meetingRecordMapper.SetSignInTime(String.valueOf(new Date().getTime()), username, meetingid) == 1) {
//            设置签到时间成功
//            接下来继续设置该会议参会人数自增
            meetingMapper.AddCountsByMeetingid(meetingid);
            meetingRecordMapper.ChangeMeetingStatusByUsernameAndMeetingId(1, username, meetingid);
            if (meetingMapper.GetMeetingByMeetingid(meetingid).getCounts() == 1) {
//                当前为第一个加入会议 开始会议 设置会议状态为进行中 并且设置当前时间为会议开始时间
                meetingMapper.UpdateMeetingStatusByMeetingid(1, meetingid);
                meetingMapper.UpdateMeetingStartTimeByMeetingid(String.valueOf(new Date().getTime()), meetingid);
            }
            ResMap.put("message", "签到成功！");
            ResMap.put("status", 1);
            return ResMap;
        } else {
            ResMap.put("message", "签到失败！");
            ResMap.put("status", 0);
            return ResMap;
        }
    }

    //    用以定时任务更新
    @Override
    public void InitMeeting() {
        // 定义HashMap接收结果
//        HashMap<String, Object> ResMap = new HashMap<>();
//        初始化会议
//        [当前时间晚于会议结束时间，设置会议状态为2 设置在会人数为0]
        List<Meeting> meetings = meetingMapper.GetAllMeeting();
        for (Meeting meeting : meetings) {
//           获取当前时间
            String nowtimestamp = String.valueOf(new Date().getTime());
//            获取结束时间
            String finishtimestamp = meeting.getFinishtimestamp();
            if (Long.parseLong(nowtimestamp) > Long.parseLong(finishtimestamp)) {
                meeting.setMeetingstatus(2);
                meeting.setCounts(0);
                meetingMapper.UpdateMeetingByMeeting(meeting);
                meetingRecordMapper.ChangeMeetingRecordStatusByMeetingId(0,meeting.getMeetingid());
            }
        }
//        初始化会议记录
        List<MeetingRecord> meetingRecords = meetingRecordMapper.GetAllMeetingRecord();
        for (MeetingRecord meetingRecord:meetingRecords)
        {
//           获取当前时间
            String nowtimestamp = String.valueOf(new Date().getTime());
        }
    }

}

