package com.zero.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

/**
 * @program: AiMeeting
 * @description: 描述会议列表对象的实体类
 * @author: 周俊宇
 * @create: 2021-07-01 11:44
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MeetingListEntity{
//    会议id
    private String meetingid;
//    定义会议名称
    private String meetingname;
//    会议创建者
    private String createusername;
//    会议计划开始时间
    private String createtimestamp;
//    会议计划结束时间
    private String finishtimestamp;
//    会议状态
    private Integer meetingstatus;
//    定义会议地址
    private String meetingaddress;
//    该会议显示图标
    private String icon;
//    重写equals和hashcode方法以保证每个会议在 HashSet中的唯一性
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MeetingListEntity that = (MeetingListEntity) o;
        return Objects.equals(meetingid, that.meetingid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(meetingid);
    }
}
