package com.zero.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: AiMeeting
 * @description: 会议实体类
 * @author: 周俊宇
 * @create: 2021-06-29 09:02
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Meeting {
    //    id
    private Integer id;
    //    会议编号
    private String meetingid;
    //    会议创建开始时间
    private String createtimestamp;
    //    会议创建结束时间
    private String finishtimestamp;
    //    会议正式开始时间
    private String starttimestamp;
    //    会议正式结束时间
    private String endtimestamp;
    //    会议创建者
    private String createusername;
    //    会议当前状态
    private Integer meetingstatus;
    //    会议人数
    private Integer counts = 0;
    //    会议名称
    private String meetingname;
    // 会议所在房间号
    private Integer roomid;
}
