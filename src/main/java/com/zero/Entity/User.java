package com.zero.Entity;

import lombok.Data;

@Data
public class User {
//    用户id
    private Integer id;
//    用户名
    private String username;
//    用户密码
    private String password;
//    用户权限
    private String permission;
//    用户角色
    private String role;
//    用户封禁状态
    private Integer ban;
//    用户真实姓名
    private String realname;
}
