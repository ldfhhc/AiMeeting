package com.zero.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: AiMeeting
 * @description: 会议实体类
 * @author: 周俊宇
 * @create: 2021-06-29 09:02
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MeetingRecord {
//    id
    private Integer id;

//    参会者用户名
    private String username;

//    加入会议时间
    private String jointimestamp;

//    签到时间
    private String signintimestamp;

//    签退时间
    private String signouttimestamp;

//    参会id
    private String meetingid;

//    参会状态 (0:已退会 1:参会中)
    private Integer recordstatus=0;
}
