package com.zero.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: AiMeeting
 * @description: 语音注释实体类
 * @author: 周俊宇
 * @create: 2021-07-11 20:25
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Media {
    private Integer id;
    private String mediavalue;
    private String meetingid;
    private String username;
    private String createtimestamp;
    private String icon;
}
