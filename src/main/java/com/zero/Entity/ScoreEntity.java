package com.zero.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: AiMeeting
 * @description: 描述人脸匹配相似度
 * @author: 周俊宇
 * @create: 2021-06-28 13:49
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScoreEntity implements Comparable{
    private String username;
    private Double score;


    @Override
    public int compareTo(Object o) {
        if (o instanceof ScoreEntity)
        {
            ScoreEntity s = (ScoreEntity)o;
            if (this.score < s.getScore())
            {
                return 1;
            }else if(this.score == s.getScore())
            {
                return 0;
            }else{
                return -1;
            }
        }
        return 0;
    }
}
