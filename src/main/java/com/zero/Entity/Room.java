package com.zero.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @program: AiMeeting
 * @description: 房间实体类
 * @author: 周俊宇
 * @create: 2021-07-09 11:16
 **/
@Data
@AllArgsConstructor
public class Room {
    private Integer id;
    private Integer roomid;
    private String roomname;

}
