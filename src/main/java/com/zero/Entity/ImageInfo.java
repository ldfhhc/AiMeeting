package com.zero.Entity;

import lombok.Data;

/**
 * @program: AiMeeting
 * @description: 图片实体类
 * @author: 周俊宇
 * @create: 2021-06-24 13:10
 **/
@Data
public class ImageInfo {
    private Integer id;
    private String username;
    private String img;
}
