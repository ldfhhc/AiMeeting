import com.zero.Entity.MeetingListEntity;
import com.zero.Entity.Room;
import com.zero.util.FaceUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @program: AiMeeting
 * @description: 测试类
 * @author: 周俊宇
 * @create: 2021-06-28 10:46
 **/
public class FaceLoginTest {
//    public static void main(String[] args) {
//        MeetingListEntity meetingListEntity1 = new MeetingListEntity("1","1","1","1","!",1);
//        MeetingListEntity meetingListEntity2 = new MeetingListEntity("1","1","1","1","!",2);
//        MeetingListEntity meetingListEntity3 = new MeetingListEntity("1","1","1","1","!",0);
//        HashSet<MeetingListEntity> hashSet = new HashSet<>();
//        hashSet.add(meetingListEntity1);
//        hashSet.add(meetingListEntity2);
//        hashSet.add(meetingListEntity3);
//        System.out.println(hashSet);
//        System.out.println(hashSet.size());
//        System.out.println(meetingListEntity1==meetingListEntity2);
//    }
public static void main(String[] args) {
    Room room1 = new Room(1,101,"101");
    Room room2 = new Room(2,102,"102");
    Room room3 = new Room(3,103,"103");
    Room room4 = new Room(4,104,"104");
    Room room5 = new Room(5,105,"105");
    Room target = new Room(5,105,"105");
    List<Room> roomList = new ArrayList<>();
    roomList.add(room1);
    roomList.add(room2);
    roomList.add(room3);
    roomList.add(room4);
    roomList.add(room5);
    for (Room room:roomList)
    {
        System.out.println(room.getId() == target.getId() && room.getRoomid() == target.getRoomid() && room.getRoomname().equals(target.getRoomname()));
    }

// 测试 contains
    System.out.println("测试contains");
    System.out.println(roomList.contains(target));
}
}
